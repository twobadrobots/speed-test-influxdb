import argparse
import datetime
import json
import logging
import os
import re
import sys
import time

import configargparse
from influxdb import InfluxDBClient

logging.basicConfig()
logger = logging.getLogger(__name__)

def main(options: argparse.Namespace):


    speedtest_results = {}
    if options.tests in ["all", "ookla"]:
        speedtest_output = ""
        while True:
            logger.debug("Starting ookla speedtest")
            stream = os.popen("speedtest --accept-license --accept-gdpr -f json")
            lines = stream.readlines()
            logger.debug("returned %d lines:\n%s" % (len(lines), lines))
            lines = [l for l in lines if "Try again" not in l]
            logger.debug("Filtered lines %s" % lines)
            if len(lines) > 0:
                speedtest_output = lines[-1]
                break
            time.sleep(5)

        logger.debug("result %s" % speedtest_output)
        speedtest_output_results = json.loads(speedtest_output)
        speedtest_results = dict(
            timestamp=datetime.datetime.strptime(speedtest_output_results["timestamp"], "%Y-%m-%dT%H:%M:%SZ").replace(microsecond=0),
            source="ookla",
            ping=speedtest_output_results["ping"]["latency"],
            down=speedtest_output_results["download"]["bandwidth"]/1e5,
            up=speedtest_output_results["upload"]["bandwidth"]/1e5
        )



    btwholesale_results = {}
    if options.tests in ["all", "bt"]:
        logger.debug("Starting btwholesale speedtest")
        stream = os.popen("%s %s/auto-speed-test/auto-speed-test.py" % (sys.executable, sys.path[0]))
        btwholesale_output = stream.read()
        btwholesale_numbers = re.findall('([0-9\.]+)', btwholesale_output)
        btwholesale_results = dict(
            timestamp=datetime.datetime.utcnow().replace(microsecond=0),
            source="bt",
            ping=btwholesale_numbers[0],
            down=btwholesale_numbers[1],
            up=btwholesale_numbers[2]
        )

    for result in [x for x in [speedtest_results, btwholesale_results] if x != {}]:
        logger.debug('"' + result["timestamp"].isoformat() + '","' + result["source"] + '",' + ','.join([str(result[k]) for k in ["ping", "down", "up"]]))

    db_client = InfluxDBClient(
        host=options.db_host,
        database=options.db_database,
        ssl=False
    )

    db_client.create_database(options.db_database)

    points = []
    for timestamp, source, ping, down, up in [speedtest_results.values(), btwholesale_results.values()]:
        points.extend([
            {
                "measurement": f"{source}_ping",
                "tags": {
                    "provider": source,
                    "units": "ms"
                },
                "time": timestamp.isoformat(),
                "fields": {
                    "value": float(ping)
                }
            },
            {
                "measurement": f"{source}_down",
                "tags": {
                    "provider": source,
                    "units": "mbps"
                },
                "time": timestamp.isoformat(),
                "fields": {
                    "value": float(down)
                }
            },
            {
                "measurement": f"{source}_up",
                "tags": {
                    "provider": source,
                    "units": "mbps"
                },
                "time": timestamp.isoformat(),
                "fields": {
                    "value": float(up)
                }
            }
        ])


    if not options.dry_run:
        db_client.write_points(points)

if __name__ == "__main__":
    p = configargparse.ArgParser(default_config_files=['config.ini'])

    p.add('-c', '--config', is_config_file=True, help='config file path')
    p.add('-l', '--log-level', default="info", help='level of logging')
    p.add('-d', '--db-host', required=True, help='the hostname or ip of the target database')
    p.add('-p', '--db-port', default="8086", help='the db port to use')
    p.add('-n', '--db-database', default="wanmetrics", help='the database name to use')
    p.add('-t', '--tests', default="all", choices={"all", "bt", "ookla"}, help='which tests to run')
    p.add('-x', '--dry-run', default=False, action='store_true', help='whether to upload metrics')

    options = p.parse_args()
    logger.setLevel(options.log_level.upper())
    logger.debug(f"{options=}")
    main(options)
