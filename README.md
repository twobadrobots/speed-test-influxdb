# Speed Test InfluxDB

Periodically gather WAN statistics (Up, Down, Ping) from a couple of sources and send them to an influx instance.

## Install

I've been running this on python 3.11 but i'm sure that isn't the minimum version required.

For the BT Wholesale speedtest, this uses a fork of a repo from github, `git submodule init` and `git submodule update` will pull it down, you can disable the need for this by requesting only the Ookla test `--tests ookla`.

I recommend creating a venv via `python3 -m venv .venv`

General dependencies can be installed with `.venv/bin/pip install -r requirements.txt`

You can install influxdb via `apt install influxdb` or spin up a container with:  
`docker run -d -v $PWD/influxdb:/var/lib/influxdb -p 8086:8086 --name influxdb influxdb:1.8`

## Usage 

To trigger speed tests and send measurements to influx run: `.venv/bin/python3 main.py -d [influxdb host or ip]`

There is a cronable script you can add your token to; `cron/wan-metrics`.

I've included a basic Grafana dashboard, you'll need to find and replace the datasource UUIDs with your configured datasource UUIDs  
`docker run -d -v $PWD/grafana: -p 3000:3000 --name grafana grafana/grafana`

![Grafana Dashboard](screenshot.png "Grafana Dashboard")